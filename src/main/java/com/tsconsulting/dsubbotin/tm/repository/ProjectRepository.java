package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.Date;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public boolean existById(final String userId, final String id) throws AbstractException {
        return findById(userId, id) != null;
    }

    @Override
    public Project findByName(final String userId, final String name) throws AbstractException {
        for (Project project : entities) {
            if (name.equals(project.getName())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public void removeByName(final String userId, final String name) throws AbstractException {
        final Project project = findByName(userId, name);
        entities.remove(project);
    }

    @Override
    public void updateBuyId(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws AbstractException {
        final Project project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    public void updateByIndex(
            final String userId,
            final int index,
            final String name,
            final String description
    ) throws AbstractException {
        final Project project = findByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    public void startById(final String userId, final String id) throws AbstractException {
        final Project project = findById(userId, id);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
    }

    @Override
    public void startByIndex(final String userId, final int index) throws AbstractException {
        final Project project = findByIndex(userId, index);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
    }

    @Override
    public void startByName(final String userId, final String name) throws AbstractException {
        final Project project = findByName(userId, name);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
    }

    @Override
    public void finishById(final String userId, final String id) throws AbstractException {
        final Project project = findById(userId, id);
        project.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByIndex(final String userId, final int index) throws AbstractException {
        final Project project = findByIndex(userId, index);
        project.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByName(final String userId, final String name) throws AbstractException {
        final Project project = findByName(userId, name);
        project.setStatus(Status.COMPLETED);
    }

    @Override
    public void updateStatusById(
            final String userId,
            final String id,
            final Status status
    ) throws AbstractException {
        final Project project = findById(userId, id);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
    }

    @Override
    public void updateStatusByIndex(
            final String userId,
            final int index,
            final Status status
    ) throws AbstractException {
        final Project project = findByIndex(userId, index);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
    }

    @Override
    public void updateStatusByName(
            final String userId,
            final String name,
            final Status status
    ) throws AbstractException {
        final Project project = findByName(userId, name);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
    }

}