package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.remove(entity);
    }

    @Override
    public void clear(final String userId) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.clear();
    }

    @Override
    public List<E> findAll(final String userId) {
        List<E> ownerEntities = new ArrayList<>();
        for (E entity : entities) {
            if (userId.equals(entity.getUserId())) ownerEntities.add(entity);
        }
        return ownerEntities;
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.sort(comparator);
        return ownerEntities;
    }

    @Override
    public E findById(final String userId, final String id) throws AbstractException {
        List<E> ownerEntities = findAll(userId);
        for (E entity : ownerEntities) {
            if (id.equals(entity.getId())) return entity;
        }
        throw new EntityNotFoundException();
    }

    @Override
    public E findByIndex(final String userId, final int index) throws AbstractException {
        List<E> ownerEntities = findAll(userId);
        if (ownerEntities.size() - 1 < index) throw new IndexIncorrectException();
        return ownerEntities.get(index);
    }

    @Override
    public E removeById(final String userId, final String id) throws AbstractException {
        List<E> ownerEntities = findAll(userId);
        final E entity = findById(userId, id);
        ownerEntities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final int index) throws AbstractException {
        List<E> ownerEntities = findAll(userId);
        final E entity = findByIndex(userId, index);
        ownerEntities.remove(entity);
        return entity;
    }

}