package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public boolean existById(final String userId, final String id) throws AbstractException {
        return findById(userId, id) != null;
    }

    @Override
    public Task findByName(final String userId, final String name) throws AbstractException {
        for (Task task : entities) {
            if (name.equals(task.getName())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public void removeByName(final String userId, final String name) throws AbstractException {
        final Task task = findByName(userId, name);
        entities.remove(task);
    }

    @Override
    public void updateBuyId(final String userId, final String id,
                            final String name,
                            final String description) throws AbstractException {
        final Task task = findById(userId, id);
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void updateBuyIndex(final String userId, final int index,
                               final String name,
                               final String description) throws AbstractException {
        final Task task = findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void startById(final String userId, final String id) throws AbstractException {
        final Task task = findById(userId, id);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
    }

    @Override
    public void startByIndex(final String userId, final int index) throws AbstractException {
        final Task task = findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
    }

    @Override
    public void startByName(final String userId, final String name) throws AbstractException {
        final Task task = findByName(userId, name);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
    }

    @Override
    public void finishById(final String userId, final String id) throws AbstractException {
        final Task task = findById(userId, id);
        task.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByIndex(final String userId, final int index) throws AbstractException {
        final Task task = findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByName(final String userId, final String name) throws AbstractException {
        final Task task = findByName(userId, name);
        task.setStatus(Status.COMPLETED);
    }

    @Override
    public void updateStatusById(
            final String userId,
            final String id,
            final Status status
    ) throws AbstractException {
        final Task task = findById(userId, id);
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
    }

    @Override
    public void updateStatusByIndex(
            final String userId,
            final int index,
            final Status status
    ) throws AbstractException {
        final Task task = findByIndex(userId, index);
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
    }

    @Override
    public void updateStatusByName(
            final String userId,
            final String name,
            final Status status
    ) throws AbstractException {
        final Task task = findByName(userId, name);
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
    }

    @Override
    public void bindTaskToProjectById(
            final String userId,
            final String projectId,
            final String taskId
    ) throws AbstractException {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskById(final String userId, final String id) throws AbstractException {
        final Task task = findById(userId, id);
        task.setProjectId(null);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String id) throws AbstractException {
        final List<Task> taskList = new ArrayList<>();
        for (Task task : entities) {
            if (id.equals(task.getProjectId())) taskList.add(task);
        }
        if (taskList.size() == 0) throw new TaskNotFoundException();
        return taskList;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String id) {
        for (Task task : entities) {
            if (id.equals(task.getProjectId())) task.setProjectId(null);
        }
    }

}
