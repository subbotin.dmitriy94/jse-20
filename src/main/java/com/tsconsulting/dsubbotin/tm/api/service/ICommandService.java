package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownCommandException;

import java.util.Collection;

public interface ICommandService {

    AbstractCommand getCommandByName(String name) throws AbstractException;

    AbstractCommand getCommandByArg(String arg) throws AbstractException;

    Collection<AbstractCommand> getCommands();

    Collection<AbstractSystemCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void add(AbstractCommand command) throws UnknownCommandException;

}
