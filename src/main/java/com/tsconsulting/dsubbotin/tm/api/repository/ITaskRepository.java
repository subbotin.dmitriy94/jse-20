package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    boolean existById(String userId, String id) throws AbstractException;

    Task findByName(String userId, String name) throws AbstractException;

    void removeByName(String userId, String name) throws AbstractException;

    void updateBuyId(String userId, String id, String name, String description) throws AbstractException;

    void updateBuyIndex(String userId, int index, String name, String description) throws AbstractException;

    void startById(String userId, String id) throws AbstractException;

    void startByIndex(String userId, int index) throws AbstractException;

    void startByName(String userId, String name) throws AbstractException;

    void finishById(String userId, String id) throws AbstractException;

    void finishByIndex(String userId, int index) throws AbstractException;

    void finishByName(String userId, String name) throws AbstractException;

    void updateStatusById(String userId, String id, Status status) throws AbstractException;

    void updateStatusByIndex(String userId, int index, Status status) throws AbstractException;

    void updateStatusByName(String userId, String name, Status status) throws AbstractException;

    void bindTaskToProjectById(String userId, String projectId, String taskId) throws AbstractException;

    void unbindTaskById(String userId, String id) throws AbstractException;

    List<Task> findAllByProjectId(String userId, String id) throws AbstractException;

    void removeAllTaskByProjectId(String userId, String id);

}
