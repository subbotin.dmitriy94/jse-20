package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;

public interface IUserService extends IService<User> {

    void create(String login, String password) throws AbstractException;

    void create(String login,
                String password,
                Role role) throws AbstractException;

    void create(String login,
                String password,
                Role role,
                String email) throws AbstractException;

    User findByLogin(String login) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    void setPassword(String id, String password) throws AbstractException;

    User setRole(String id, Role role) throws AbstractException;

    User updateById(String id,
                    String lastName,
                    String firstName,
                    String middleName,
                    String email) throws AbstractException;

    boolean isLogin(String login) throws AbstractException;

}
