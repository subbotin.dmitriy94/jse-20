package com.tsconsulting.dsubbotin.tm.command;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public abstract String arg();

    @Override
    public String toString() {
        String result = super.toString();
        final String arg = arg();
        if (arg != null && !arg.isEmpty()) result += " (" + arg + ")";
        return result;
    }

}
