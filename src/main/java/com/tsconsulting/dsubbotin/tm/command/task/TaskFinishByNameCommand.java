package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "Finish task by name.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskService().finishByName(currentUserId, name);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
