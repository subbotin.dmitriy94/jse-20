package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String description();

    public abstract void execute() throws AbstractException;

    @Override
    public String toString() {
        StringBuilder sbCommandToString = new StringBuilder();
        final String name = name();
        final String description = description();
        if (!EmptyUtil.isEmpty(name)) sbCommandToString.append(name);
        if (!EmptyUtil.isEmpty(description)) sbCommandToString.append(" - ").append(description);
        return sbCommandToString.toString();
    }

}
