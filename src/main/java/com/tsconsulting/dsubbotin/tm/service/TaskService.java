package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String userId, final String name) throws AbstractException {
        checkName(name);
        Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(
            final String userId,
            final String name,
            final String description
    ) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) throws AbstractException {
        checkIndex(index);
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    public Task findByName(final String userId, final String name) throws AbstractException {
        checkName(name);
        return taskRepository.findByName(userId, name);
    }

    @Override
    public void removeByName(final String userId, final String name) throws AbstractException {
        checkName(name);
        taskRepository.removeByName(userId, name);
    }

    @Override
    public void updateById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws AbstractException {
        checkId(id);
        checkName(name);
        taskRepository.updateBuyId(userId, id, name, description);
    }

    @Override
    public void updateByIndex(
            final String userId,
            final int index,
            final String name,
            final String description
    ) throws AbstractException {
        checkIndex(index);
        checkName(name);
        taskRepository.updateBuyIndex(userId, index, name, description);
    }

    @Override
    public void startById(final String userId, final String id) throws AbstractException {
        checkId(id);
        taskRepository.startById(userId, id);
    }

    @Override
    public void startByIndex(final String userId, final int index) throws AbstractException {
        checkIndex(index);
        taskRepository.startByIndex(userId, index);
    }

    @Override
    public void startByName(final String userId, final String name) throws AbstractException {
        checkName(name);
        taskRepository.startByName(userId, name);
    }

    @Override
    public void finishById(final String userId, final String id) throws AbstractException {
        checkId(id);
        taskRepository.finishById(userId, id);
    }

    @Override
    public void finishByIndex(final String userId, final int index) throws AbstractException {
        checkIndex(index);
        taskRepository.finishByIndex(userId, index);
    }

    @Override
    public void finishByName(final String userId, final String name) throws AbstractException {
        checkName(name);
        taskRepository.finishByName(userId, name);
    }

    @Override
    public void updateStatusById(
            final String userId,
            final String id,
            final Status status
    ) throws AbstractException {
        checkId(id);
        taskRepository.updateStatusById(userId, id, status);
    }

    @Override
    public void updateStatusByIndex(
            final String userId,
            final int index,
            final Status status
    ) throws AbstractException {
        checkIndex(index);
        taskRepository.updateStatusByIndex(userId, index, status);
    }

    @Override
    public void updateStatusByName(
            final String userId,
            final String name,
            final Status status
    ) throws AbstractException {
        checkName(name);
        taskRepository.updateStatusByName(userId, name, status);
    }

    private void checkName(final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

    private void checkId(final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

}